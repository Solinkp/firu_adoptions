# firu_adoptions

A new Flutter project for pet adoptions, user based project.

## SDK
- Flutter 3.3.4

## How to run the project
+ Execute the following commands:
- flutter pub get
- flutter pub run build_runner build --delete-conflicting-outputs
- Run in debug or non-debug mode

## Build project
