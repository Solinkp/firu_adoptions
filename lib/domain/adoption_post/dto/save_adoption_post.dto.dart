class SaveAdoptionPostDto {
  final String owner;
  final String phone;
  final bool whatsapp;
  final String petId;
  final DateTime createdAt;

  SaveAdoptionPostDto({
    required this.owner,
    required this.phone,
    required this.whatsapp,
    required this.petId,
    required this.createdAt,
  });
}
