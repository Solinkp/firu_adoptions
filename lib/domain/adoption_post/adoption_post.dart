import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:firu_adoptions/domain/pet/pet.dart';

part 'adoption_post.freezed.dart';
part 'adoption_post.g.dart';

@freezed
class AdoptionPost with _$AdoptionPost {
  const factory AdoptionPost({
    required String owner,
    required String phone,
    required bool whatsapp,
    required Pet pet,
  }) = _AdoptionPost;

  factory AdoptionPost.fromJson(Map<String, dynamic> json) => _$AdoptionPostFromJson(json);
}
