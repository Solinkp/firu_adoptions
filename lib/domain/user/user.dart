import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';
part 'user.g.dart';

@freezed
class FiruUser with _$FiruUser {
  const factory FiruUser({
    String? id,
    required String fullName,
    required String phone,
    required bool whatsapp,
    required String email,
    required DateTime createdAt,
  }) = _FiruUser;

  factory FiruUser.fromJson(Map<String, dynamic> json) => _$FiruUserFromJson(json);
}
