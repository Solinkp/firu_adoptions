import 'package:freezed_annotation/freezed_annotation.dart';

part 'pet.freezed.dart';
part 'pet.g.dart';

@freezed
class Pet with _$Pet {
  const factory Pet({
    required bool sex, // true = male - false = female
    required bool specie, // true = dog - false = cat
    required String detpKey,
    required String details,
    required List<String> pictures,
    String? id,
    String? name,
    DateTime? birthdate,
    DateTime? createdAt,
  }) = _Pet;

  factory Pet.fromJson(Map<String, dynamic> json) => _$PetFromJson(json);
}
