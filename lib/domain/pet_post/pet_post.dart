import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'pet_post.freezed.dart';
part 'pet_post.g.dart';

@freezed
class PetPost with _$PetPost {
  const factory PetPost({
    required bool specie, // true = dog - false = cat
    required List<String> pictures,
    String? id,
    String? name,
  }) = _PetPost;

  factory PetPost.fromJson(Map<String, dynamic> json) => _$PetPostFromJson(json);

  factory PetPost.fromDocument(DocumentSnapshot<Map<String, dynamic>> doc) {
    return PetPost.fromJson(doc.data()!).copyWith(id: doc.id);
  }
}
