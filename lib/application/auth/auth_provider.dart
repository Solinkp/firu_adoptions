import 'package:injectable/injectable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:firu_adoptions/di/injection.dart';
import 'package:firu_adoptions/domain/user/user.dart';
import 'package:firu_adoptions/infrastructure/auth/repository/i_auth_repository.dart';

final authProvider = Provider.autoDispose(
  (ref) => locator<AuthNotifier>(),
);

final authStateProvider = StreamProvider.autoDispose<User?>(
  (ref) => ref.read(authProvider).authStateChange,
);

@injectable
class AuthNotifier {
  final IAuthRepository _repository;

  AuthNotifier(this._repository);

  Stream<User?> get authStateChange => _repository.authStateChange;

  Future<void> signOut() async => _repository.signOut();

  Future<void> sendPasswordResetEmail({required String email}) async =>
      _repository.sendPasswordResetEmail(email: email);

  Future<String?> signInWithEmailAndPassword({required String email, required String password}) async {
    String? result = await _repository.signInWithEmailAndPassword(email: email, password: password);
    return result;
  }

  Future<String?> signUpWithEmailAndPassword({
    required String email,
    required String password,
    required FiruUser userInfo,
  }) async {
    String? result = await _repository.signUpWithEmailAndPassword(
      email: email,
      password: password,
      userInfo: userInfo,
    );
    return result;
  }
}
