// import 'package:injectable/injectable.dart';
// import 'package:flutter_riverpod/flutter_riverpod.dart';

// import 'package:firu_adoptions/di/injection.dart';

// final postProvider = Provider.autoDispose(
//   (ref) => locator<PostNotifier>(),
// );


// @injectable
// class PostNotifier {
//   final IPostRepository _repository;

//   PostNotifier(this._repository);

//   Future<String?> getPost(String petId) async {
//     String? result = await _repository.getPost(petId);
//     if (result != null) {
//       return result;
//     }
//   }
// }
