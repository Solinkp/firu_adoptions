import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:firu_adoptions/di/injection.dart';
import 'package:firu_adoptions/utils/constants/constants.dart';

final settingsProvider = Provider.autoDispose(
  (ref) => locator<SettingsNotifier>(),
);

final defaultSpeciePrefProvider = StateProvider<int>(
    (ref) => ref.read(settingsProvider).getDefaultPetSpeciePref());

final defaultLanguagePrefProvider = StateProvider<String>(
    (ref) => ref.read(settingsProvider).getDefaultLanguagePref());

@injectable
class SettingsNotifier {
  SettingsNotifier();

  /// Gets
  int getDefaultPetSpeciePref() {
    return Hive.box(hivePreferenceBox).get(preferencePetSpecie) ?? 0;
  }

  String getDefaultLanguagePref() {
    return Hive.box(hivePreferenceBox).get(preferenceLanguage) ?? 'es';
  }

  /// Puts
  void setDefaultDepartmentPref(String newDepartmentPref) {
    Hive.box(hivePreferenceBox).put(preferenceDept, newDepartmentPref);
  }

  void setDefaultPetSpeciePref(int newSpeciePref) {
    Hive.box(hivePreferenceBox).put(preferencePetSpecie, newSpeciePref);
  }

  void setDefaultLanguagePref(String newLanguagePref) {
    Hive.box(hivePreferenceBox).put(preferenceLanguage, newLanguagePref);
  }
}
