import 'package:injectable/injectable.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:firu_adoptions/di/injection.dart';
import 'package:firu_adoptions/domain/pet_post/pet_post.dart';
import 'package:firu_adoptions/infrastructure/pet/repository/i_pet_repository.dart';

final timelineProvider = Provider.autoDispose(
  (ref) => locator<TimelineNotifier>(),
);

final deptFilterProvider = StateProvider<String>((_) => '');

@injectable
class TimelineNotifier {
  final IPetRepository _repository;

  TimelineNotifier(this._repository);

  Stream<List<PetPost>> getPetPosts({required String deptKey, required int specieKey}) {
    return _repository.getPetPosts(deptKey, specieKey);
  }
}
