import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/generated/l10n.dart';
import 'package:firu_adoptions/domain/pet_post/pet_post.dart';
import 'package:firu_adoptions/utils/constants/constants.dart';
import 'package:firu_adoptions/presentation/theme/custom_colors.dart';
import 'package:firu_adoptions/application/settings/settings_provider.dart';
import 'package:firu_adoptions/application/timeline/timeline_provider.dart';
import 'package:firu_adoptions/presentation/timeline/widgets/post_item.dart';
import 'package:firu_adoptions/presentation/widgets/general_widgets/loader.dart';
import 'package:firu_adoptions/presentation/timeline/widgets/timeline_sheet.dart';
import 'package:firu_adoptions/presentation/timeline/widgets/timeline_filter.dart';
import 'package:firu_adoptions/presentation/widgets/general_widgets/data_warning_message.dart';

class Timeline extends ConsumerStatefulWidget {
  const Timeline({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _TimelineState();
}

class _TimelineState extends ConsumerState<Timeline> {
  String _dept = Hive.box(hivePreferenceBox).get(preferenceDept);
  int _specie = 0;

  @override
  Widget build(BuildContext context) {
    final timelineWatcher = ref.watch(timelineProvider);
    final deptWatcher = ref.watch(deptFilterProvider);
    final specieWatcher = ref.watch(defaultSpeciePrefProvider);

    return Column(
      children: [
        SizedBox(
          height: kToolbarHeight,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 20.w),
                child: Text(
                  S.of(context).appTitle,
                  style: Theme.of(context)
                      .textTheme
                      .headline6!
                      .copyWith(fontFamily: 'Courgette'),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(right: 20.w),
                child: IconButton(
                  onPressed: _openFilterSheet,
                  icon: const Icon(Icons.filter_list_alt),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: StreamBuilder(
            stream: timelineWatcher.getPetPosts(
              deptKey: deptWatcher,
              specieKey: specieWatcher,
            ),
            builder: (context, AsyncSnapshot<List<PetPost>> snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.waiting:
                  return const Loader();
                default:
                  if (snapshot.hasData) {
                    if (snapshot.data!.isEmpty) {
                      return DataWarningMessage(
                        message: S.of(context).emptyData,
                      );
                    }
                    return _timelinePosts(snapshot.data!);
                  } else if (snapshot.hasError) {
                    return DataWarningMessage(message: S.of(context).errorData);
                  } else {
                    return const Loader();
                  }
              }
            },
          ),
        ),
      ],
    );
  }

  Widget _timelinePosts(List<PetPost> posts) {
    return GridView.builder(
      itemCount: posts.length,
      shrinkWrap: true,
      physics: const BouncingScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
      ),
      padding: EdgeInsets.symmetric(horizontal: 15.0.w),
      itemBuilder: (context, index) {
        return PostItem(petPost: posts[index]);
      },
    );
  }

  void _openFilterSheet() {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => TimelineSheet(
        openDepartmentFilter: _openDeptFilterModal,
        openSpecieFilter: _openSpecieFilterModal,
      ),
    );
  }

  void _openDeptFilterModal() {
    TimelineFilter.showFilterDialog(
      context: context,
      title: S.of(context).filterDept,
      radioList: _getRadioListDepts(),
    );
  }

  List<Widget> _getRadioListDepts() {
    List<Widget> depts = [];
    for (String dept in nicDepartments) {
      depts.add(
        RadioListTile(
          value: dept,
          groupValue: _dept,
          title: Text(dept),
          onChanged: (selectedDept) => _setSelectedDept(
            selectedDept.toString(),
          ),
          selected: _dept == dept,
          activeColor: CustomColors.customBrown,
        ),
      );
    }
    return depts;
  }

  void _setSelectedDept(String newDept) {
    setState(() {
      _dept = newDept;
    });
    ref.read(deptFilterProvider.notifier).state = _dept;
    Navigator.of(context).pop();
  }

  void _openSpecieFilterModal() {
    TimelineFilter.showFilterDialog(
      context: context,
      title: S.of(context).filterSpecie,
      radioList: _getRadioListSpecies(),
    );
  }

  List<Widget> _getRadioListSpecies() {
    List<Widget> species = [];
    for (Map<String, int> specie in petSpecies) {
      species.add(
        RadioListTile(
          value: specie.values.first,
          groupValue: _specie,
          title: Text(petSpeciesText[specie.values.first]),
          onChanged: (selectedSpecie) => _setSelectedSpecie(
            int.parse(selectedSpecie.toString()),
          ),
          selected: _specie == specie.values.first,
          activeColor: CustomColors.customBrown,
        ),
      );
    }
    return species;
  }

  void _setSelectedSpecie(int newSpecie) {
    setState(() {
      _specie = newSpecie;
    });
    ref.read(settingsProvider).setDefaultPetSpeciePref(_specie);
    ref.read(defaultSpeciePrefProvider.notifier).state = _specie;
    Navigator.of(context).pop();
  }
}
