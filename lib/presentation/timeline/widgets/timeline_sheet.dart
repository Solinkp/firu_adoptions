import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/generated/l10n.dart';
import 'package:firu_adoptions/presentation/theme/custom_colors.dart';

class TimelineSheet extends StatelessWidget {
  final VoidCallback openDepartmentFilter;
  final VoidCallback openSpecieFilter;

  const TimelineSheet({
    Key? key,
    required this.openDepartmentFilter,
    required this.openSpecieFilter,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
      backgroundColor: CustomColors.customDark,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
      ),
      onClosing: () {},
      builder: (context) {
        return SizedBox(
          height: 300.h,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.min,
            children: [
              _sheetTile(
                icon: const Icon(Icons.location_city_rounded),
                title: S.of(context).filterDept,
                action: () => {
                  Navigator.of(context).pop(),
                  openDepartmentFilter(),
                },
              ),
              _sheetTile(
                icon: const Icon(Icons.pets),
                title: S.of(context).filterSpecie,
                action: () => {
                  Navigator.of(context).pop(),
                  openSpecieFilter(),
                },
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _sheetTile({
    required Icon icon,
    required String title,
    required VoidCallback action,
  }) {
    return ListTile(
      iconColor: CustomColors.customBackground,
      textColor: CustomColors.customBackground,
      style: ListTileStyle.drawer,
      leading: icon,
      title: Text(title),
      trailing: const Icon(Icons.arrow_forward_ios_rounded),
      onTap: action,
    );
  }
}
