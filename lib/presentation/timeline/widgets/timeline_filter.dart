import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/generated/l10n.dart';
import 'package:firu_adoptions/presentation/theme/custom_colors.dart';

class TimelineFilter {
  static showFilterDialog({
    required BuildContext context,
    required String title,
    required List<Widget> radioList,
  }) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          backgroundColor: CustomColors.customBackground,
          scrollable: true,
          contentPadding: const EdgeInsets.all(5),
          titleTextStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontSize: 32.sp,
                color: CustomColors.customDark,
                decoration: TextDecoration.underline,
              ),
          title: Center(child: Text(title)),
          content: Column(children: radioList),
          actions: [
            ElevatedButton(
              child: Text(S.of(context).cancel),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        );
      },
    );
  }
}
