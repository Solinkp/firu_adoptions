import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:card_swiper/card_swiper.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/domain/pet_post/pet_post.dart';
import 'package:firu_adoptions/utils/constants/constants.dart';
import 'package:firu_adoptions/presentation/router/router.gr.dart';
import 'package:firu_adoptions/presentation/theme/custom_colors.dart';

class PostItem extends StatelessWidget {
  final PetPost petPost;

  const PostItem({
    Key? key,
    required this.petPost,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5.0.w),
      child: InkWell(
        borderRadius: BorderRadius.circular(20),
        highlightColor: Colors.transparent,
        splashColor: CustomColors.customBrown,
        onTap: () => _displayMoreInfo(context),
        child: Stack(
          children: [
            Center(
              child: Container(
                margin: EdgeInsets.all(5.0.w),
                decoration: BoxDecoration(
                  color: CustomColors.customDark,
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(color: CustomColors.customYellow, width: 3),
                ),
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(17)),
                  child: Swiper(
                    itemCount: petPost.pictures.length,
                    scrollDirection: Axis.horizontal,
                    physics: const BouncingScrollPhysics(),
                    loop: true,
                    pagination: const SwiperPagination(),
                    itemBuilder: (context, index) => FadeInImage(
                      fit: BoxFit.cover,
                      placeholder: const AssetImage(assetFiruLogoClear),
                      image: NetworkImage(petPost.pictures[index]),
                      imageErrorBuilder: (BuildContext context, Object exception, StackTrace? stackTrace) {
                        return Image.asset(assetFiruLogoClear);
                      },
                    ),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                decoration: BoxDecoration(
                  color: CustomColors.customBrown,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: CustomColors.customYellow, width: 2),
                ),
                child: Text(
                  petPost.name ?? 'N/D',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 24.sp),
                  textAlign: TextAlign.center,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _displayMoreInfo(BuildContext context) {
    context.router.push(AdoptionPost(petId: petPost.id!));
  }
}
