import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:firu_adoptions/generated/l10n.dart';
import 'package:firu_adoptions/application/auth/auth_provider.dart';
import 'package:firu_adoptions/presentation/profile/widgets/profile.dart';
import 'package:firu_adoptions/presentation/profile/widgets/not_logged_in.dart';
import 'package:firu_adoptions/presentation/widgets/general_widgets/loader.dart';
import 'package:firu_adoptions/presentation/widgets/general_widgets/data_warning_message.dart';

class ProfileScreen extends ConsumerWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final authProviderWatcher = ref.watch(authStateProvider);

    return authProviderWatcher.when(
      data: (data) => data == null ? const NotLoggedIn() : const Profile(),
      error: (error, stack) => DataWarningMessage(
        message: S.of(context).errorData,
      ),
      loading: () => const Loader(),
    );
  }
}
