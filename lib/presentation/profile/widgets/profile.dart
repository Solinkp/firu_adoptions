import 'package:firu_adoptions/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:firu_adoptions/application/auth/auth_provider.dart';

class Profile extends ConsumerStatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  ConsumerState<Profile> createState() => _ProfileState();
}

class _ProfileState extends ConsumerState<Profile> {
  @override
  Widget build(BuildContext context) {
    final authProviderWatcher = ref.watch(authProvider);

    return Column(
      children: [
        /// header with icons?
        Container(
          height: kToolbarHeight,
          color: Colors.green,
        ),
        Expanded(
          child: Center(
            child: ElevatedButton(
              onPressed: () {
                authProviderWatcher.signOut();
              },
              child: Text(S.of(context).logoutButton),
            ),
          ),
        ),
      ],
    );
  }
}
