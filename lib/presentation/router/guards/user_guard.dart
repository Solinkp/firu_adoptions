import 'package:hive/hive.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';

import 'package:firu_adoptions/utils/globals.dart';
import 'package:firu_adoptions/generated/l10n.dart';
import 'package:firu_adoptions/utils/constants/constants.dart';
import 'package:firu_adoptions/presentation/router/router.gr.dart';
import 'package:firu_adoptions/infrastructure/user/adapter/user_adapter.dart';

class CheckIfUserHasLoggedIn extends AutoRouteGuard {
  @override
  void onNavigation(NavigationResolver resolver, StackRouter router) {
    final userBox = Hive.box<UserHive>(hiveUserBox);

    if (userBox.values.isNotEmpty && userBox.values.last.id.isNotEmpty) {
      resolver.next(true);
    } else {
      snackbarKey.currentState?.showSnackBar(
        SnackBar(content: Text(S.current.loginRequiredForPost)),
      );
      router.push(Login(nextRoute: resolver.route.name));
    }
  }
}
