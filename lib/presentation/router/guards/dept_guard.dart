import 'package:hive/hive.dart';
import 'package:auto_route/auto_route.dart';

import 'package:firu_adoptions/utils/constants/constants.dart';
import 'package:firu_adoptions/presentation/router/router.gr.dart';

class CheckIfUserHasDeptKey extends AutoRouteGuard {
  @override
  void onNavigation(NavigationResolver resolver, StackRouter router) {
    final String department = Hive.box(hivePreferenceBox).get(
      preferenceDept,
      defaultValue: '',
    );

    if (department.isNotEmpty) {
      resolver.next(true);
    } else {
      router.push(const Welcome());
    }
  }
}
