import 'package:auto_route/auto_route.dart';

import 'package:firu_adoptions/presentation/welcome/welcome_screen.dart';
import 'package:firu_adoptions/presentation/auth/login/login_screen.dart';
import 'package:firu_adoptions/presentation/new_post/new_post_screen.dart';
import 'package:firu_adoptions/presentation/router/guards/user_guard.dart';
import 'package:firu_adoptions/presentation/router/guards/dept_guard.dart';
import 'package:firu_adoptions/presentation/auth/signup/signup_screen.dart';
import 'package:firu_adoptions/presentation/dashboard/dashboard_screen.dart';
import 'package:firu_adoptions/presentation/adoption_post/adoption_post_screen.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route,Screen',
  routes: [
    CustomRoute(
      path: '/login/:nextRoute',
      name: 'login',
      page: LoginScreen,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      path: '/signup',
      name: 'signUp',
      page: SignupScreen,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      path: '/welcome',
      name: 'welcome',
      page: WelcomeScreen,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      initial: true,
      path: '/dashboard',
      name: 'dashboard',
      page: DashboardScreen,
      guards: [CheckIfUserHasDeptKey],
      transitionsBuilder: TransitionsBuilders.slideLeft,
    ),
    CustomRoute(
      path: '/adoptionPost',
      name: 'adoptionPost',
      page: AdoptionPostScreen,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      path: '/newPost',
      name: 'newPost',
      page: NewPostScreen,
      guards: [CheckIfUserHasLoggedIn],
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
  ],
)
class $AppRouter {}
