import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/generated/l10n.dart';
import 'package:firu_adoptions/domain/user/user.dart';
import 'package:firu_adoptions/utils/connectivity_helper.dart';
import 'package:firu_adoptions/application/auth/auth_provider.dart';
import 'package:firu_adoptions/presentation/layouts/auth_layout.dart';
import 'package:firu_adoptions/utils/extensions/string_extensions.dart';
import 'package:firu_adoptions/presentation/widgets/form_widgets/custom_checkbox.dart';
import 'package:firu_adoptions/presentation/widgets/form_widgets/custom_text_field.dart';
import 'package:firu_adoptions/presentation/widgets/form_widgets/normal_submit_button.dart';

class SignupScreen extends ConsumerStatefulWidget {
  const SignupScreen({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _SignupScreenState();
}

class _SignupScreenState extends ConsumerState<SignupScreen> {
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  late TextEditingController _emailController;
  late TextEditingController _passwordController;
  late TextEditingController _nameController;
  late TextEditingController _phoneController;
  bool _whatsapp = false;

  @override
  void initState() {
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    _nameController = TextEditingController();
    _phoneController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _nameController.dispose();
    _phoneController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final authProviderWatcher = ref.watch(authProvider);

    return AuthLayout(
      body: Container(
        padding: EdgeInsets.only(bottom: 20.h),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Text(
                S.of(context).signupTitle,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 32.sp),
              ),
              CustomTextFormField(
                keyboardType: TextInputType.text,
                labelText: S.of(context).name,
                textController: _nameController,
                icon: Icons.person,
                validator: (value) => value?.validateEmpty(context),
              ),
              SizedBox(height: 10.h),
              CustomTextFormField(
                keyboardType: TextInputType.phone,
                labelText: S.of(context).phone,
                textController: _phoneController,
                icon: Icons.phone,
                charLength: 8,
                validator: (value) => value?.validateEmpty(context),
              ),
              SizedBox(height: 20.h),
              CustomCheckbox(
                value: _whatsapp,
                title: S.of(context).whatsapp,
                onChanged: (value) => setState(() => _whatsapp = value!),
              ),
              CustomTextFormField(
                keyboardType: TextInputType.emailAddress,
                labelText: S.of(context).email,
                hintText: S.of(context).emailHint,
                textController: _emailController,
                icon: Icons.email,
                validator: (value) => value?.validateEmail(context),
              ),
              SizedBox(height: 10.h),
              CustomTextFormField(
                keyboardType: TextInputType.text,
                labelText: S.of(context).password,
                textController: _passwordController,
                last: true,
                obscured: true,
                icon: Icons.lock,
                validator: (value) => value?.validateEmpty(context),
              ),
              SizedBox(height: 80.h),
              NormalSubmitButton(
                onPressed: () => _signUp(authProviderWatcher),
                text: S.of(context).signupButton,
                isLoading: _isLoading,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _signUp(AuthNotifier authProviderWatcher) async {
    if (await Connectivity().checkConnection) {
      if (_formKey.currentState!.validate()) {
        setState(() {
          _isLoading = true;
        });
        FiruUser firuUser = FiruUser(
          fullName: _nameController.text.trim(),
          phone: _phoneController.text.trim(),
          whatsapp: _whatsapp,
          email: _emailController.text.trim(),
          createdAt: DateTime.now(),
        );

        authProviderWatcher
            .signUpWithEmailAndPassword(
              email: _emailController.text.trim(),
              password: _passwordController.text.trim(),
              userInfo: firuUser,
            )
            .then(_processSignUpResult);
      }
    }
  }

  void _processSignUpResult(String? result) {
    setState(() {
      _isLoading = false;
    });
    if (result != null && result.isNotEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(result),
      ));
      if (S.current.authVerifyEmail == result) {
        context.router.pop();
      }
    }
  }
}
