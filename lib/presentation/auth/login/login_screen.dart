import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/generated/l10n.dart';
import 'package:firu_adoptions/utils/connectivity_helper.dart';
import 'package:firu_adoptions/presentation/router/router.gr.dart';
import 'package:firu_adoptions/application/auth/auth_provider.dart';
import 'package:firu_adoptions/presentation/layouts/auth_layout.dart';
import 'package:firu_adoptions/presentation/theme/custom_colors.dart';
import 'package:firu_adoptions/utils/extensions/string_extensions.dart';
import 'package:firu_adoptions/presentation/widgets/form_widgets/custom_text_field.dart';
import 'package:firu_adoptions/presentation/widgets/form_widgets/normal_submit_button.dart';

class LoginScreen extends ConsumerStatefulWidget {
  final String nextRoute;

  const LoginScreen({
    Key? key,
    @PathParam() required this.nextRoute,
  }) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _LoginScreenState();
}

class _LoginScreenState extends ConsumerState<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  late TextEditingController _emailController;
  late TextEditingController _passwordController;

  @override
  void initState() {
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final authProviderWatcher = ref.watch(authProvider);

    return AuthLayout(
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 30.h, bottom: 20.h),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  CustomTextFormField(
                    keyboardType: TextInputType.emailAddress,
                    labelText: S.of(context).email,
                    hintText: S.of(context).emailHint,
                    textController: _emailController,
                    icon: Icons.email,
                    validator: (value) => value?.validateEmail(context),
                  ),
                  SizedBox(height: 40.h),
                  CustomTextFormField(
                    keyboardType: TextInputType.text,
                    labelText: S.of(context).password,
                    textController: _passwordController,
                    last: true,
                    obscured: true,
                    icon: Icons.lock,
                    validator: (value) => value?.validateEmpty(context),
                  ),
                  SizedBox(height: 100.h),
                  NormalSubmitButton(
                    onPressed: () => _login(authProviderWatcher),
                    text: S.of(context).loginButton,
                    isLoading: _isLoading,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(30.w),
            child: Center(
              child: RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: S.of(context).signupQuestion,
                      style: const TextStyle(color: CustomColors.customDark),
                    ),
                    TextSpan(
                      text: S.of(context).signupQuestionLink,
                      style: const TextStyle(
                        color: CustomColors.customBrown,
                        fontWeight: FontWeight.bold,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = _goToSignUpScreen,
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  void _goToSignUpScreen() {
    context.router.push(const SignUp());
  }

  Future<void> _login(AuthNotifier authProviderWatcher) async {
    if (await Connectivity().checkConnection) {
      if (_formKey.currentState!.validate()) {
        setState(() {
          _isLoading = true;
        });
        authProviderWatcher
            .signInWithEmailAndPassword(
              email: _emailController.text.trim(),
              password: _passwordController.text.trim(),
            )
            .then(_processLoginResult);
      }
    }
  }

  Future<void> _processLoginResult(String? result) async {
    if (result != null && result.isNotEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(result),
      ));
    } else {
      if (widget.nextRoute == 'Profile') {
        context.router.pop();
      } else {
        context.router.popAndPush(const NewPost());
      }
    }
    if (mounted) {
      setState(() {
        _isLoading = false;
      });
    }
  }
}
