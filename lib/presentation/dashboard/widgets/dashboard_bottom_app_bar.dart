import 'package:firu_adoptions/generated/l10n.dart';
import 'package:flutter/material.dart';

import 'package:firu_adoptions/presentation/theme/custom_colors.dart';

class DashboardBottomAppBar extends StatelessWidget {
  final int screen;
  final Function(int) changeScreenFunc;

  const DashboardBottomAppBar({
    Key? key,
    required this.screen,
    required this.changeScreenFunc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      shape: const CircularNotchedRectangle(),
      color: Theme.of(context).primaryColor,
      elevation: 0,
      child: SizedBox(
        width: double.infinity,
        height: kToolbarHeight,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            _menuButton(
              index: 0,
              icon: screen == 0 ? Icons.home_rounded : Icons.home_outlined,
              tooltip: S.of(context).home,
            ),
            const SizedBox.shrink(),
            _menuButton(
              index: 1,
              icon: screen == 1 ? Icons.person_rounded : Icons.person_outlined,
              tooltip: S.of(context).account,
            ),
          ],
        ),
      ),
    );
  }

  Widget _menuButton({
    required int index,
    required IconData icon,
    required String tooltip,
  }) {
    return IconButton(
      tooltip: tooltip,
      splashColor: CustomColors.customDark,
      highlightColor: CustomColors.customDark,
      onPressed: () => changeScreenFunc(index),
      icon: Icon(icon),
    );
  }
}
