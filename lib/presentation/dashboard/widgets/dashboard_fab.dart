import 'package:flutter_svg/svg.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/utils/constants/constants.dart';
import 'package:firu_adoptions/presentation/router/router.gr.dart';
import 'package:firu_adoptions/presentation/theme/custom_colors.dart';

class DashboardFab extends StatelessWidget {
  const DashboardFab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 110.0.w,
      width: 110.0.w,
      child: FloatingActionButton(
        backgroundColor: CustomColors.customDark,
        onPressed: () => _openNewPost(context),
        child: SvgPicture.asset(
          assetFiruLogoClearSvg,
          height: 90.0.w,
          fit: BoxFit.scaleDown,
        ),
      ),
    );
  }

  _openNewPost(BuildContext context) {
    context.router.push(const NewPost());
  }
}
