import 'package:flutter/material.dart';

import 'package:firu_adoptions/presentation/timeline/timeline.dart';
import 'package:firu_adoptions/presentation/profile/profile_screen.dart';
import 'package:firu_adoptions/presentation/layouts/dashboard_layout.dart';
import 'package:firu_adoptions/presentation/dashboard/widgets/dashboard_fab.dart';
import 'package:firu_adoptions/presentation/dashboard/widgets/dashboard_bottom_app_bar.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  final List<Widget> screens = const [
    Timeline(),
    ProfileScreen(),
  ];
  int currentScreen = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: DashboardLayout(
        body: screens[currentScreen],
        fab: const DashboardFab(),
        bottomAppBar: DashboardBottomAppBar(
          screen: currentScreen,
          changeScreenFunc: _changeScreen,
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    if (currentScreen == 1) {
      _changeScreen(0);
      return false;
    }
    return true;
  }

  void _changeScreen(int index) {
    if (currentScreen != index) {
      setState(() {
        currentScreen = index;
      });
    }
  }
}
