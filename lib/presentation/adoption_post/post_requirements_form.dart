import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/presentation/widgets/form_widgets/custom_text_field.dart';

class PostRequirementsForm extends StatefulWidget {
  final bool isPreview;

  const PostRequirementsForm({
    Key? key,
    this.isPreview = false,
  }) : super(key: key);

  @override
  State<PostRequirementsForm> createState() => _PostRequirementsFormState();
}

class _PostRequirementsFormState extends State<PostRequirementsForm> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController _previewController;

  @override
  void initState() {
    _previewController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _previewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          CustomTextFormField(
            isReadOnly: widget.isPreview,
            border: true,
            labelText: 'Full name',
            keyboardType: TextInputType.name,
            textController: _previewController,
          ),
          _inputPair([
            CustomTextFormField(
              isReadOnly: widget.isPreview,
              border: true,
              labelText: 'telefono',
              keyboardType: TextInputType.phone,
              textController: _previewController,
            ),
            CustomTextFormField(
              isReadOnly: widget.isPreview,
              border: true,
              labelText: 'Departamento',
              keyboardType: TextInputType.text,
              textController: _previewController,
            ),
          ]),
          CustomTextFormField(
            isReadOnly: widget.isPreview,
            border: true,
            labelText: 'Address',
            keyboardType: TextInputType.multiline,
            lines: 4,
            textController: _previewController,
          ),

          ///
          /// tipo de casa radio trio -> apartamento - casa - otro

          ///
          /// radio alquila esta vivienda?
          /// if yes
          /// _inputPair([]) -> landlord's name and phone

          /// radio do you plan on moving in the next 12 months
          /// if yes
          /// what do you plan to do with the pet? textfield

          /// why do you want this pet? textfield

          /// radio - have you previuously owned pets?
          /// if yes
          /// list all current or previous pets you have had in the last 10 years
          /// name - specie - breed - sex - spayed(esterilizad@)/neutered(castrad@)? - current on vaccinations? - do you still own it? if not, why?

          /// radio - are there any children in your house?
          /// if yes
          /// what ages?

          /// radio - do you have a fence? o porche para que no se salga la pet

          /// how many hours will this pet be left alone during the day?

          /// where will this pet be kept wile you are out of town?

          /// radio - Are you willing to provide your pet with medicine and vaccinations at your own expense?

          /// Who will be financially responsile for all the medical costs?

          /// List any characteristics of an animal that would NOT fit with you, your family or lifestyle
        ],
      ),
    );
  }

  Widget _inputPair(List<Widget> pair) {
    return Row(
      children: [
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(right: 10.w),
            child: pair[0],
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 10.w),
            child: pair[1],
          ),
        ),
      ],
    );
  }
}
