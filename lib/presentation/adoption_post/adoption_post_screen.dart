import 'package:flutter/material.dart';
// import 'package:card_swiper/card_swiper.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

// import 'package:firu_adoptions/presentation/theme/custom_colors.dart';
import 'package:firu_adoptions/presentation/layouts/post_layout.dart';

class AdoptionPostScreen extends ConsumerStatefulWidget {
  final String petId;

  const AdoptionPostScreen({
    Key? key,
    required this.petId,
  }) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _AdoptionPostScreenState();
}

class _AdoptionPostScreenState extends ConsumerState<AdoptionPostScreen> {
  @override
  Widget build(BuildContext context) {
    // final adoptionPostWatcher = ref.watch(adoptionPostProvider);

    // return StreamBuilder(
    // stream: adoptionPostWatcher.getPost(petId: widget.petId),
    //   builder: (context, AsyncSnapshot<Post> snapshot) {
    //     switch (snapshot.connectionState) {
    //       case ConnectionState.waiting:
    //         return const PostLayout(isLoading: true);
    //       default:
    //         if (snapshot.hasData) {
    //           // return _timelinePosts(snapshot.data!);
    //           return PostLayout(
    //             isLoading: false,
    //             petName: '',
    //             postHeader: _postHeader(),
    //             postBody: _postBody(),
    //           );
    //         } else {
    //           return const PostLayout(isLoading: true);
    //         }
    //     }
    //   }
    // );

    return const PostLayout(
      isLoading: true,
    );
  }

  // Widget _postHeader() {
  //   return Swiper(
  //     itemCount: 2,
  //     scrollDirection: Axis.horizontal,
  //     physics: const BouncingScrollPhysics(),
  //     loop: true,
  //     pagination: const SwiperPagination(
  //       builder: DotSwiperPaginationBuilder(
  //         activeColor: CustomColors.customBackground,
  //         color: CustomColors.customDark,
  //       ),
  //     ),
  //     itemBuilder: (context, index) => Text(
  //       widget.petId,
  //     ),
  //   );
  // }

  // Widget _postBody() {
  //   return Column(
  //     children: const [],
  //   );
  // }
}
