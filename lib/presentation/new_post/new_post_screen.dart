import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/generated/l10n.dart';
import 'package:firu_adoptions/utils/connectivity_helper.dart';
import 'package:firu_adoptions/utils/constants/constants.dart';
import 'package:firu_adoptions/utils/extensions/string_extensions.dart';
import 'package:firu_adoptions/presentation/layouts/new_post_layout.dart';
import 'package:firu_adoptions/presentation/new_post/widgets/contact_method.dart';
import 'package:firu_adoptions/presentation/widgets/form_widgets/custom_text_field.dart';
import 'package:firu_adoptions/presentation/widgets/form_widgets/custom_date_picker.dart';
import 'package:firu_adoptions/presentation/widgets/form_widgets/custom_image_picker.dart';
import 'package:firu_adoptions/presentation/widgets/form_widgets/normal_submit_button.dart';
import 'package:firu_adoptions/presentation/widgets/form_widgets/custom_radio_list_duo.dart';

class NewPostScreen extends ConsumerStatefulWidget {
  const NewPostScreen({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _NewPostScreenState();
}

class _NewPostScreenState extends ConsumerState<NewPostScreen> {
  final _formKey = GlobalKey<FormState>();
  final double _imageSize = 140;
  final List<File> _pictureList = [];
  // late String _pictureOnePath;
  // late String _pictureTwoPath;
  late TextEditingController _nameController;
  late TextEditingController _breedController;
  late TextEditingController _ageController;
  late TextEditingController _descriptionController;
  bool _specie = true;
  bool _sex = true;
  ContactMethodEnum _contactMethod = ContactMethodEnum.phone;
  bool _isLoading = false;

  @override
  void initState() {
    // _pictureOnePath = '';
    // _pictureTwoPath = '';
    _nameController = TextEditingController();
    _breedController = TextEditingController();
    _ageController = TextEditingController();
    _descriptionController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _breedController.dispose();
    _ageController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return NewPostLayout(
      newPostBody: Container(
        padding: EdgeInsets.only(bottom: 20.h),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: _imagePickers(),
              ),
              const Padding(padding: EdgeInsets.only(top: 20.0)),
              _inputPair([
                CustomTextFormField(
                  border: true,
                  labelText: S.of(context).name,
                  keyboardType: TextInputType.text,
                  textController: _nameController,
                  validator: (value) => value?.validateEmpty(context),
                ),
                CustomTextFormField(
                  border: true,
                  labelText: S.of(context).breed,
                  keyboardType: TextInputType.text,
                  textController: _breedController,
                  validator: (value) => value?.validateEmpty(context),
                ),
              ]),
              CustomDatePicker(
                dateController: _ageController,
                labelText: S.of(context).petBirthdate,
                validator: (value) => value?.validateEmpty(context),
              ),
              CustomTextFormField(
                border: true,
                labelText: S.of(context).description,
                keyboardType: TextInputType.multiline,
                lines: 4,
                textController: _descriptionController,
                last: true,
                validator: (value) => value?.validateEmpty(context),
              ),
              CustomRadioListDuo(
                groupTitle: S.of(context).selectSpecie,
                radioOneTitle: S.of(context).dog,
                radioTwoTitle: S.of(context).cat,
                radioOneValue: true,
                radioTwoValue: false,
                groupValue: _specie,
                onRadioChange: _onSpecieRadioChange,
              ),
              CustomRadioListDuo(
                groupTitle: S.of(context).selectSex,
                radioOneTitle: S.of(context).male,
                radioTwoTitle: S.of(context).female,
                radioOneValue: true,
                radioTwoValue: false,
                groupValue: _sex,
                onRadioChange: _onSexRadioChange,
              ),
              ContactMethod(
                groupValue: _contactMethod,
                onRadioChange: _onContactMethodChange,
              ),
              SizedBox(height: 40.h),
              NormalSubmitButton(
                onPressed: _submitPost,
                text: S.of(context).submitPost,
                isLoading: _isLoading,
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _imagePickers() {
    List<Widget> list = [];
    File? fileOne = _pictureList.asMap()[0];
    File? fileTwo = _pictureList.asMap()[1];

    list.add(
      CustomImagePicker(
        boxSize: _imageSize,
        index: 0,
        setImageFromSource: _setImage,
        removeImageFromSource: fileOne == null ? null : _removeImage,
        picture: fileOne,
      ),
    );
    if (fileOne != null) {
      list.add(
        CustomImagePicker(
          boxSize: _imageSize,
          index: 1,
          setImageFromSource: _setImage,
          removeImageFromSource: fileTwo == null ? null : _removeImage,
          picture: fileTwo,
        ),
      );
    }
    return list;
  }

  void _setImage(XFile? image, int index) async {
    if (image != null) {
      File? file = _pictureList.asMap()[index];
      if (file != null) {
        _pictureList.removeAt(index);
      }
      setState(() {
        _pictureList.insert(index, File(image.path));
      });
    }
  }

  void _removeImage(int index) {
    setState(() {
      _pictureList.removeAt(index);
    });
  }

  Widget _inputPair(List<Widget> pair) {
    return Row(
      children: [
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(right: 10.w),
            child: pair[0],
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 10.w),
            child: pair[1],
          ),
        ),
      ],
    );
  }

  void _onSpecieRadioChange(Object? value) {
    setState(() {
      _specie = value as bool;
    });
  }

  void _onSexRadioChange(Object? value) {
    setState(() {
      _sex = value as bool;
    });
  }

  void _onContactMethodChange(Object? value) {
    setState(() {
      _contactMethod = value as ContactMethodEnum;
    });
  }

  void _submitPost() async {
    if (await Connectivity().checkConnection) {
      if (_formKey.currentState!.validate()) {
        setLoading(true);

        ///
        setLoading(false);
      }
    }
  }

  void setLoading(bool value) {
    setState(() {
      _isLoading = value;
    });
  }
}
