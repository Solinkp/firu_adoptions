import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/generated/l10n.dart';
import 'package:firu_adoptions/utils/constants/constants.dart';
import 'package:firu_adoptions/presentation/theme/custom_colors.dart';
import 'package:firu_adoptions/presentation/adoption_post/post_requirements_form.dart';
import 'package:firu_adoptions/presentation/widgets/general_widgets/custom_text_button.dart';

class ContactMethod extends StatelessWidget {
  final ContactMethodEnum groupValue;
  final Function(Object?) onRadioChange;

  const ContactMethod({
    Key? key,
    required this.groupValue,
    required this.onRadioChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 15.0),
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        border: Border.all(
          color: CustomColors.customBrown,
          width: 2,
        ),
      ),
      child: Column(
        children: [
          Center(
            child: Text(
              S.of(context).contactMethodTitle,
              style: TextStyle(fontSize: 30.sp),
              textAlign: TextAlign.center,
            ),
          ),
          _radioList(
            context,
            S.of(context).contactByPhone,
            ContactMethodEnum.phone,
          ),
          _radioList(
            context,
            S.of(context).contactByForm,
            ContactMethodEnum.adoptionForm,
          ),
        ],
      ),
    );
  }

  Widget _radioList(BuildContext context, String title, ContactMethodEnum value) {
    return RadioListTile(
      secondary: value == ContactMethodEnum.adoptionForm
          ? IconButton(
              onPressed: () => _showAdoptionFormPreview(context),
              icon: const Icon(
                Icons.remove_red_eye_rounded,
                color: CustomColors.customBrown,
              ),
            )
          : null,
      title: Text(title),
      groupValue: groupValue,
      value: value,
      onChanged: onRadioChange,
      activeColor: CustomColors.customBrown,
    );
  }

  void _showAdoptionFormPreview(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          backgroundColor: CustomColors.customBackground,
          title: Center(
            child: Text(
              S.of(context).previewFormTitle,
              style: TextStyle(fontSize: 32.sp),
            ),
          ),
          content: const SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: PostRequirementsForm(isPreview: true),
          ),
          actions: [
            CustomTextButton(
              text: S.of(context).close,
              fontColor: CustomColors.customBrown,
              pressCallback: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
