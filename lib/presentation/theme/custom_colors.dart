import 'package:flutter/material.dart';

class CustomColors {
  static const customBackground = Color(0xFFe7e2cb);
  static const customBrown = Color(0xFFd09942);
  static const customDark = Color(0xff35363b);
  static const customDarkGreen = Color(0xFF93b1a2);
  //
  static const customYellow = Color(0xFFe7d24d);
  static const customOrange = Color(0xFFd85401);
  static const customLightGreen = Color(0xFF9fe2bf);
  static const customLightBlue = Color(0xFF96dfdb);
}
