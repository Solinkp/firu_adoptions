import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/presentation/theme/custom_colors.dart';

class AppTheme {
  static final ThemeData lightTheme = ThemeData(
    primaryColor: CustomColors.customBrown,
    colorScheme: const ColorScheme.light(
      primary: CustomColors.customBrown,
      onPrimary: Colors.white,
      secondary: CustomColors.customDark,
      onSecondary: CustomColors.customBackground,
    ),
    scaffoldBackgroundColor: CustomColors.customBackground,
    splashColor: CustomColors.customBrown,
    brightness: Brightness.light,
    textTheme: TextTheme(
      headline5: TextStyle(
        fontSize: 44.sp,
        color: Colors.white,
      ),
      headline6: TextStyle(
        fontSize: 44.sp,
        color: CustomColors.customDark,
      ),
      bodyText1: TextStyle(
        fontSize: 26.sp,
        color: Colors.white,
      ),
      bodyText2: TextStyle(
        fontSize: 36.sp,
        color: CustomColors.customDark,
      ),
    ),
    fontFamily: 'Quicksand',
    visualDensity: VisualDensity.adaptivePlatformDensity,
  );
}
