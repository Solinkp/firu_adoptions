import 'package:hive/hive.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/generated/l10n.dart';
import 'package:firu_adoptions/utils/constants/constants.dart';
import 'package:firu_adoptions/presentation/router/router.gr.dart';
import 'package:firu_adoptions/presentation/theme/custom_colors.dart';
import 'package:firu_adoptions/presentation/layouts/welcome_layout.dart';
import 'package:firu_adoptions/presentation/widgets/general_widgets/firu_logo.dart';
import 'package:firu_adoptions/presentation/widgets/form_widgets/custom_dropdown.dart';
import 'package:firu_adoptions/presentation/widgets/form_widgets/small_submit_button.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  String? _department;

  @override
  Widget build(BuildContext context) {
    return WelcomeLayout(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 30.0.w),
        child: Column(
          children: [
            const Expanded(
              flex: 2,
              child: Center(
                child: FiruLogo(
                  color: CustomColors.customBackground,
                  radius: 140.0,
                  imageAsset: assetFiruLogoSvg,
                ),
              ),
            ),
            Expanded(
              flex: 4,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    S.of(context).welcome,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                  CustomDropdown(
                    value: _department,
                    label: S.of(context).selectDept,
                    items: _getDepts(),
                    validatorFunction: _dropDownvalidatorFunction,
                    setOnChangedValue: _onChangedDropDownValue,
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: SmallSubmitButton(
                text: S.of(context).continueAction,
                pressCallback: _nextAction,
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<DropdownMenuItem<String>> _getDepts() {
    List<DropdownMenuItem<String>> list = [];
    for (String dept in nicDepartments) {
      list.add(DropdownMenuItem(value: dept, child: Text(dept)));
    }
    return list;
  }

  String? _dropDownvalidatorFunction(dynamic value) {
    return value == null ? S.of(context).selectDept : null;
  }

  void _onChangedDropDownValue(String? value) {
    setState(() {
      _department = value;
    });
  }

  Future<void> _nextAction() async {
    if (_department != null) {
      await Hive.box(hivePreferenceBox).put(preferenceDept, _department);
      context.router.pushAndPopUntil(
        const Dashboard(),
        predicate: (Route<dynamic> route) => false,
      );
    }
  }
}
