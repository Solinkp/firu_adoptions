import 'package:flutter/material.dart';

import 'package:firu_adoptions/presentation/theme/custom_colors.dart';

class CustomTextButton extends StatelessWidget {
  final String text;
  final VoidCallback pressCallback;
  final Color fontColor;

  const CustomTextButton({
    Key? key,
    required this.text,
    required this.pressCallback,
    required this.fontColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: pressCallback,
      style: TextButton.styleFrom(
        foregroundColor: CustomColors.customBackground,
      ),
      child: Text(text, style: TextStyle(color: fontColor)),
    );
  }
}
