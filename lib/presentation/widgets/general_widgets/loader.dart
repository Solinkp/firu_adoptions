import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/presentation/theme/custom_colors.dart';

class Loader extends StatelessWidget {
  final bool isCircle;

  const Loader({
    Key? key,
    this.isCircle = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: isCircle
          ? SpinKitCircle(
              key: const Key('spinkitLoader'),
              color: CustomColors.customBackground,
              size: 100.w,
            )
          : SpinKitChasingDots(
              key: const Key('spinkitLoader'),
              color: CustomColors.customBrown,
              size: 200.w,
            ),
    );
  }
}
