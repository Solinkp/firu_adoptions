import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FiruLogo extends StatelessWidget {
  final Color color;
  final double radius;
  final String imageAsset;

  const FiruLogo({
    Key? key,
    required this.color,
    required this.radius,
    required this.imageAsset,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'hero_firu_logo',
      child: Padding(
        padding: const EdgeInsets.all(0.0),
        child: CircleAvatar(
          backgroundColor: color,
          radius: radius.w,
          child: SvgPicture.asset(imageAsset),
        ),
      ),
    );
  }
}
