import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DataWarningMessage extends StatelessWidget {
  final String message;

  const DataWarningMessage({
    Key? key,
    required this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      key: const Key('dataWarningMessage'),
      child: Padding(
        padding: EdgeInsets.all(100.0.w),
        child: Text(
          message,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
