import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/presentation/widgets/general_widgets/loader.dart';

class NormalSubmitButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  final bool isLoading;

  const NormalSubmitButton({
    Key? key,
    required this.onPressed,
    required this.text,
    required this.isLoading,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 80.h,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
        ),
        onPressed: isLoading ? () {} : onPressed,
        child: Align(
          alignment: Alignment.center,
          child: isLoading
              ? const Loader(isCircle: true)
              : Text(
                  text,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
        ),
      ),
    );
  }
}
