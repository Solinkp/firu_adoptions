import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/presentation/theme/custom_colors.dart';

class CustomDropdown extends StatefulWidget {
  final String? value;
  final String label;
  final List<DropdownMenuItem<String>> items;
  final Function(dynamic) validatorFunction;
  final Function(String?) setOnChangedValue;

  const CustomDropdown({
    Key? key,
    this.value,
    required this.label,
    required this.items,
    required this.validatorFunction,
    required this.setOnChangedValue,
  }) : super(key: key);

  @override
  State<CustomDropdown> createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(40.0.w),
      child: DropdownButtonFormField(
        value: widget.value,
        decoration: InputDecoration(
          enabledBorder: const UnderlineInputBorder(
            borderSide: BorderSide(color: CustomColors.customBrown),
          ),
          labelText: widget.label,
          labelStyle: const TextStyle(color: CustomColors.customBrown),
          icon: const Icon(
            Icons.location_city,
            color: CustomColors.customBrown,
          ),
        ),
        dropdownColor: CustomColors.customBackground,
        iconEnabledColor: CustomColors.customBrown,
        style: TextStyle(color: CustomColors.customDark, fontSize: 26.sp),
        items: widget.items,
        validator: (value) => widget.validatorFunction(value),
        onChanged: widget.setOnChangedValue,
      ),
    );
  }
}
