import 'package:flutter/material.dart';

import 'package:firu_adoptions/presentation/theme/custom_colors.dart';

class CustomCheckbox extends StatelessWidget {
  final bool value;
  final String title;
  final void Function(bool?) onChanged;

  const CustomCheckbox({
    Key? key,
    required this.value,
    required this.title,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      value: value,
      title: Text(title),
      activeColor: CustomColors.customBrown,
      checkColor: CustomColors.customBackground,
      onChanged: onChanged,
    );
  }
}
