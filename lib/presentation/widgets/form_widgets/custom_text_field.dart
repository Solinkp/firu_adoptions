import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:firu_adoptions/presentation/theme/custom_colors.dart';

class CustomTextFormField extends StatefulWidget {
  final TextInputType keyboardType;
  final String labelText;
  final String hintText;
  final IconData? icon;
  final String? helperText;
  final bool obscured;
  final bool last;
  final int lines;
  final bool isNumber;
  final int? charLength;
  final bool isReadOnly;
  final bool filled;
  final Color filledColor;
  final Function()? onTap;
  final String? Function(String?)? validator;
  final TextEditingController textController;
  final bool border;

  const CustomTextFormField({
    Key? key,
    required this.labelText,
    required this.textController,
    this.hintText = '',
    this.keyboardType = TextInputType.text,
    this.icon,
    this.helperText,
    this.onTap,
    this.charLength,
    this.validator,
    this.lines = 1,
    this.obscured = false,
    this.last = false,
    this.isNumber = false,
    this.isReadOnly = false,
    this.filled = true,
    this.filledColor = CustomColors.customBackground,
    this.border = false,
  }) : super(key: key);

  @override
  State<CustomTextFormField> createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {
  bool _passwordInvisible = true;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 15.0),
      child: TextFormField(
        maxLines: widget.lines,
        maxLength: widget.charLength,
        keyboardType: widget.keyboardType,
        obscureText: widget.obscured ? _passwordInvisible : widget.obscured,
        controller: widget.textController,
        inputFormatters: widget.isNumber
            ? <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp(r'(^\-?\d*\.?\d*)'))
              ]
            : null,
        validator: widget.validator,
        textInputAction:
            widget.last ? TextInputAction.done : TextInputAction.next,
        readOnly: widget.isReadOnly,
        onTap: widget.onTap,
        style: widget.border
            ? const TextStyle(color: CustomColors.customDark)
            : TextStyle(color: Theme.of(context).primaryColor),
        decoration: InputDecoration(
          disabledBorder: widget.border
              ? const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                  borderSide:
                      BorderSide(color: CustomColors.customBrown, width: 2),
                )
              : const UnderlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                  borderSide:
                      BorderSide(color: CustomColors.customDark, width: 7),
                ),
          enabledBorder: widget.border
              ? const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                  borderSide:
                      BorderSide(color: CustomColors.customBrown, width: 2),
                )
              : const UnderlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                  borderSide:
                      BorderSide(color: CustomColors.customDark, width: 7),
                ),
          focusedBorder: widget.border
              ? const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                  borderSide:
                      BorderSide(color: CustomColors.customBrown, width: 2),
                )
              : const UnderlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                  borderSide:
                      BorderSide(color: CustomColors.customDark, width: 7),
                ),
          errorBorder: widget.border
              ? const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                  borderSide:
                      BorderSide(color: CustomColors.customOrange, width: 2),
                )
              : const UnderlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                  borderSide:
                      BorderSide(color: CustomColors.customOrange, width: 7),
                ),
          focusedErrorBorder: widget.border
              ? const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                  borderSide:
                      BorderSide(color: CustomColors.customOrange, width: 2),
                )
              : const UnderlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                  borderSide:
                      BorderSide(color: CustomColors.customOrange, width: 7),
                ),
          errorStyle: const TextStyle(color: CustomColors.customOrange),
          labelText: widget.labelText,
          labelStyle: widget.border
              ? const TextStyle(color: CustomColors.customDark)
              : const TextStyle(color: CustomColors.customBrown),
          hintText: widget.obscured ? '************' : widget.hintText,
          hintStyle: TextStyle(color: Theme.of(context).primaryColor),
          helperText: widget.helperText,
          prefixText:
              widget.keyboardType == TextInputType.phone ? '+505 ' : null,
          prefixIcon: widget.icon != null
              ? Icon(
                  widget.icon,
                  color: Theme.of(context).primaryColor,
                )
              : null,
          suffixIcon: widget.obscured
              ? IconButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  icon: Icon(
                    _passwordInvisible
                        ? Icons.visibility_outlined
                        : Icons.visibility_off_outlined,
                    color: Theme.of(context).primaryColor,
                  ),
                  onPressed: () => setState(
                    () => _passwordInvisible = !_passwordInvisible,
                  ),
                )
              : null,
          filled: widget.filled,
          fillColor: widget.filledColor,
        ),
      ),
    );
  }
}
