import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:firu_adoptions/presentation/theme/custom_colors.dart';

class CustomRadioListDuo extends StatelessWidget {
  final String groupTitle;
  final String radioOneTitle;
  final String radioTwoTitle;
  final bool radioOneValue;
  final bool radioTwoValue;
  final bool groupValue;
  final Function(Object?) onRadioChange;

  const CustomRadioListDuo({
    Key? key,
    required this.groupTitle,
    required this.radioOneTitle,
    required this.radioTwoTitle,
    required this.radioOneValue,
    required this.radioTwoValue,
    required this.groupValue,
    required this.onRadioChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 15.0),
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        border: Border.all(
          color: CustomColors.customBrown,
          width: 2,
        ),
      ),
      child: Column(
        children: [
          Center(
            child: Text(
              groupTitle,
              style: TextStyle(fontSize: 30.sp),
              textAlign: TextAlign.center,
            ),
          ),
          _radioList(radioOneTitle, radioOneValue),
          _radioList(radioTwoTitle, radioTwoValue),
        ],
      ),
    );
  }

  Widget _radioList(String title, bool value) {
    return RadioListTile(
      title: Text(title),
      groupValue: groupValue,
      value: value,
      onChanged: onRadioChange,
      activeColor: CustomColors.customBrown,
    );
  }
}
