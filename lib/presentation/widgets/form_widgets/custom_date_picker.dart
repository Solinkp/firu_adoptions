import 'package:flutter/material.dart';

import 'package:firu_adoptions/utils/date_formatter.dart';
import 'package:firu_adoptions/presentation/theme/custom_colors.dart';

class CustomDatePicker extends StatefulWidget {
  final String hintText;
  final String labelText;
  final TextEditingController dateController;
  final String? Function(String?)? validator;

  const CustomDatePicker({
    Key? key,
    required this.labelText,
    required this.dateController,
    this.hintText = '',
    this.validator,
  }) : super(key: key);

  @override
  State<CustomDatePicker> createState() => _CustomDatePickerState();
}

class _CustomDatePickerState extends State<CustomDatePicker> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 15.0),
      child: TextFormField(
        controller: widget.dateController,
        maxLines: 1,
        keyboardType: TextInputType.datetime,
        autofocus: false,
        cursorColor: CustomColors.customDark,
        decoration: InputDecoration(
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(16)),
            borderSide: BorderSide(color: CustomColors.customBrown, width: 2),
          ),
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(16)),
            borderSide: BorderSide(color: CustomColors.customBrown, width: 2),
          ),
          errorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(16)),
            borderSide: BorderSide(color: CustomColors.customOrange, width: 2),
          ),
          focusedErrorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(16)),
            borderSide: BorderSide(color: CustomColors.customOrange, width: 2),
          ),
          errorStyle: const TextStyle(color: CustomColors.customOrange),
          labelText: widget.labelText,
          labelStyle: const TextStyle(color: CustomColors.customDark),
          hintText: widget.hintText,
          hintStyle: TextStyle(color: Theme.of(context).primaryColor),
          prefixIcon: const Icon(Icons.cake_rounded, color: CustomColors.customDark),
        ),
        readOnly: true,
        onTap: () => _selectDate(),
        validator: widget.validator,
        onSaved: (value) => widget.dateController.text = value!,
      ),
    );
  }

  Future<void> _selectDate() async {
    final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now().subtract(const Duration(days: 30)),
      firstDate: DateTime.now().subtract(const Duration(days: 5475)),
      lastDate: DateTime.now().subtract(const Duration(days: 30)),
      builder: (_, child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: const ColorScheme.light(primary: CustomColors.customBrown),
          ),
          child: child!,
        );
      },
    );
    if (pickedDate != null) {
      widget.dateController.text = DateFormatter.getFormattedDate(pickedDate);
    }
  }
}
