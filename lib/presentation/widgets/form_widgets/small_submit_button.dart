import 'package:flutter/material.dart';

class SmallSubmitButton extends StatelessWidget {
  final String text;
  final VoidCallback pressCallback;

  const SmallSubmitButton({
    Key? key,
    required this.text,
    required this.pressCallback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: pressCallback,
      child: Text(
        text,
        style: Theme.of(context).textTheme.bodyText1,
      ),
    );
  }
}
