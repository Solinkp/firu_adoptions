import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:firu_adoptions/generated/l10n.dart';
import 'package:firu_adoptions/presentation/theme/custom_colors.dart';
import 'package:firu_adoptions/presentation/widgets/general_widgets/custom_text_button.dart';

class CustomImagePicker extends StatelessWidget {
  final double boxSize;
  final int index;
  final void Function(XFile?, int) setImageFromSource;
  final void Function(int)? removeImageFromSource;
  final File? picture;

  const CustomImagePicker({
    Key? key,
    required this.boxSize,
    required this.index,
    required this.setImageFromSource,
    required this.removeImageFromSource,
    this.picture,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: boxSize,
      width: boxSize,
      child: InkWell(
        onTap: () => _showPicturePicker(context, index),
        onLongPress: () => _removeImage(context, index),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: CustomColors.customBrown),
          ),
          child: picture != null
              ? ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  child: Image.file(
                    picture!,
                    fit: BoxFit.contain,
                  ),
                )
              : const Icon(
                  Icons.camera_alt,
                  color: Colors.grey,
                ),
        ),
      ),
    );
  }

  _showPicturePicker(BuildContext context, int index) {
    showModalBottomSheet(
      context: context,
      backgroundColor: CustomColors.customDark,
      builder: (BuildContext context) {
        return SafeArea(
          child: Wrap(
            children: [
              ListTile(
                leading: const Icon(
                  Icons.photo_library,
                  color: CustomColors.customBackground,
                ),
                title: Text(
                  S.of(context).galery,
                  style: const TextStyle(
                    color: CustomColors.customBackground,
                  ),
                ),
                onTap: () {
                  _getImage(ImageSource.gallery, index);
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                leading: const Icon(
                  Icons.camera_alt,
                  color: CustomColors.customBackground,
                ),
                title: Text(
                  S.of(context).camera,
                  style: const TextStyle(
                    color: CustomColors.customBackground,
                  ),
                ),
                onTap: () {
                  _getImage(ImageSource.camera, index);
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      },
    );
  }

  void _getImage(ImageSource source, int index) async {
    final ImagePicker picker = ImagePicker();
    XFile? image = await picker.pickImage(
      source: source,
      imageQuality: 80,
    );
    setImageFromSource(image, index);
  }

  void _removeImage(BuildContext context, int index) {
    if (removeImageFromSource != null) {
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: CustomColors.customBackground,
            title: Center(
              child: Text(S.of(context).removePictureTitle),
            ),
            content: SingleChildScrollView(
              child: Text(S.of(context).removePicture),
            ),
            actions: [
              CustomTextButton(
                text: S.of(context).cancel,
                fontColor: CustomColors.customBrown,
                pressCallback: () => Navigator.of(context).pop(),
              ),
              CustomTextButton(
                text: S.of(context).remove,
                fontColor: CustomColors.customBrown,
                pressCallback: () {
                  removeImageFromSource!(index);
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }
}
