import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ErrorMessage extends StatelessWidget {
  final String message;
  final double topPadding;
  final Color textColor;

  const ErrorMessage({
    Key? key,
    required this.message,
    required this.topPadding,
    required this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return message.isNotEmpty
        ? Container(
            padding: EdgeInsets.symmetric(vertical: topPadding),
            child: Text(
              message,
              style: TextStyle(
                fontSize: 26.0.sp,
                color: textColor,
                height: 1.0,
                fontWeight: FontWeight.w300,
              ),
            ),
          )
        : const SizedBox(height: 0);
  }
}
