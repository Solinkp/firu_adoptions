import 'package:flutter/material.dart' hide Router;
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:firu_adoptions/utils/globals.dart';
import 'package:firu_adoptions/generated/l10n.dart';
import 'package:firu_adoptions/presentation/theme/theme.dart';
import 'package:firu_adoptions/presentation/router/router.gr.dart';
import 'package:firu_adoptions/presentation/router/guards/dept_guard.dart';
import 'package:firu_adoptions/presentation/router/guards/user_guard.dart';
import 'package:firu_adoptions/application/settings/settings_provider.dart';

final _appRouter = AppRouter(
  checkIfUserHasDeptKey: CheckIfUserHasDeptKey(),
  checkIfUserHasLoggedIn: CheckIfUserHasLoggedIn(),
);

class FiruMain extends StatelessWidget {
  const FiruMain({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      builder: (context, _) => const _FiruMain(),
      minTextAdapt: true,
      designSize: const Size(750, 1334),
    );
  }
}

class _FiruMain extends ConsumerWidget {
  const _FiruMain({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final languageWatcher = ref.watch(defaultLanguagePrefProvider);

    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      scaffoldMessengerKey: snackbarKey,
      theme: AppTheme.lightTheme,
      builder: (BuildContext context, Widget? child) {
        final MediaQueryData media = MediaQuery.of(context);
        return MediaQuery(
          data: media.copyWith(
            textScaleFactor: media.textScaleFactor.clamp(1.0, 1.3),
          ),
          child: child!,
        );
      },
      locale: Locale(languageWatcher),
      onGenerateTitle: (context) => S.of(context).appTitle,
      localizationsDelegates: const [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,
      routerDelegate: _appRouter.delegate(),
      routeInformationParser: _appRouter.defaultRouteParser(),
    );
  }
}
