import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:firu_adoptions/generated/l10n.dart';
import 'package:firu_adoptions/presentation/theme/custom_colors.dart';
import 'package:firu_adoptions/presentation/widgets/general_widgets/loader.dart';

class PostLayout extends StatelessWidget {
  final bool isLoading;
  final String? petName;
  final Widget? postHeader;
  final Widget? postBody;

  const PostLayout({
    Key? key,
    required this.isLoading,
    this.petName,
    this.postHeader,
    this.postBody,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: const Key('postLayoutScaffold'),
      backgroundColor: CustomColors.customBrown,
      appBar: AppBar(
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.light,
        ),
        elevation: 0,
        centerTitle: true,
        title: Text(isLoading ? S.of(context).loading : petName ?? 'N/D'),
        backgroundColor: Colors.transparent,
      ),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light.copyWith(
          statusBarColor: Theme.of(context).scaffoldBackgroundColor,
          statusBarIconBrightness: Brightness.dark,
          systemNavigationBarColor: Theme.of(context).scaffoldBackgroundColor,
          systemNavigationBarIconBrightness: Brightness.dark,
        ),
        child: SafeArea(
          child: Stack(
            children: [
              Column(
                children: [
                  Expanded(
                    flex: 2,
                    child: postHeader ?? const SizedBox(),
                  ),
                  Expanded(
                    flex: 4,
                    child: Container(
                      width: double.infinity,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30),
                        ),
                        color: CustomColors.customBackground,
                      ),
                      child: ClipRRect(
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30),
                        ),
                        child: SingleChildScrollView(
                          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                          physics: const BouncingScrollPhysics(),
                          child: postBody,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              isLoading
                  ? const Center(
                      child: Loader(),
                    )
                  : const SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}
