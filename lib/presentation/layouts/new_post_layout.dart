import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'package:firu_adoptions/generated/l10n.dart';
import 'package:firu_adoptions/presentation/theme/custom_colors.dart';
import 'package:firu_adoptions/presentation/widgets/general_widgets/custom_text_button.dart';

class NewPostLayout extends StatelessWidget {
  final Widget newPostBody;

  const NewPostLayout({
    Key? key,
    required this.newPostBody,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: const Key('newPostLayoutScaffold'),
      appBar: AppBar(
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark,
        ),
        backgroundColor: Colors.transparent,
        foregroundColor: CustomColors.customDark,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).newPost,
          style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 38.sp),
        ),
        actions: [
          IconButton(
            color: CustomColors.customBrown,
            onPressed: () => _showPostTip(context),
            icon: const Icon(
              MdiIcons.lightbulbOnOutline,
              color: CustomColors.customDark,
            ),
          )
        ],
      ),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light.copyWith(
          systemNavigationBarColor: Theme.of(context).primaryColor,
          systemNavigationBarIconBrightness: Brightness.light,
        ),
        child: SafeArea(
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 30.0.w),
              child: newPostBody,
            ),
          ),
        ),
      ),
    );
  }

  void _showPostTip(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.customBackground,
          title: Center(
            child: Text(
              S.of(context).postTipTitle,
              style: TextStyle(fontSize: 36.sp),
            ),
          ),
          content: SingleChildScrollView(
            child: Text(S.of(context).postTip),
          ),
          actions: [
            CustomTextButton(
              text: S.of(context).close,
              fontColor: CustomColors.customBrown,
              pressCallback: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
