import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DashboardLayout extends StatelessWidget {
  final Widget bottomAppBar;
  final Widget body;
  final Widget fab;

  const DashboardLayout({
    Key? key,
    required this.bottomAppBar,
    required this.body,
    required this.fab,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: const Key('dashboardLayoutScaffold'),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light.copyWith(
          statusBarColor: Theme.of(context).scaffoldBackgroundColor,
          statusBarIconBrightness: Brightness.dark,
        ),
        child: SafeArea(child: body),
      ),
      bottomNavigationBar: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light.copyWith(
          systemNavigationBarColor: Theme.of(context).primaryColor,
          systemNavigationBarIconBrightness: Brightness.light,
        ),
        child: bottomAppBar,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: fab,
    );
  }
}
