import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:firu_adoptions/di/injection.dart' as di;
import 'package:firu_adoptions/presentation/firu_main.dart';
import 'package:firu_adoptions/utils/constants/constants.dart';
import 'package:firu_adoptions/infrastructure/user/adapter/user_adapter.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  await _initHive();
  await di.configure();

  runApp(const ProviderScope(
    child: FiruMain(),
  ));
}

Future<void> _initHive() async {
  final appDocumentDir = await getApplicationDocumentsDirectory();
  await Hive.initFlutter(appDocumentDir.path);

  /// Register adapters
  Hive.registerAdapter<UserHive>(UserAdapter());

  /// Open boxes
  await Hive.openBox(hivePreferenceBox);
  await Hive.openBox<UserHive>(hiveUserBox);
}
