import 'package:injectable/injectable.dart';

import 'package:firu_adoptions/domain/pet_post/pet_post.dart';
import 'package:firu_adoptions/infrastructure/pet/repository/i_pet_repository.dart';
import 'package:firu_adoptions/infrastructure/pet/data_source/i_pet_data_source.dart';

@LazySingleton(as: IPetRepository)
class PetRepository implements IPetRepository {
  final IPetDataSource _petDataSource;

  const PetRepository(this._petDataSource);

  @override
  Stream<List<PetPost>> getPetPosts(String deptKey, int specieKey) {
    return _petDataSource.getPetPosts(deptKey, specieKey);
  }
}
