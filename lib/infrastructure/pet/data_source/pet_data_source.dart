import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:firu_adoptions/domain/pet_post/pet_post.dart';
import 'package:firu_adoptions/utils/constants/constants.dart';
import 'package:firu_adoptions/infrastructure/pet/data_source/i_pet_data_source.dart';

@LazySingleton(as: IPetDataSource)
class PetDataSource implements IPetDataSource {
  final FirebaseFirestore _firestore;

  const PetDataSource(this._firestore);

  @override
  Stream<List<PetPost>> getPetPosts(String deptKey, int specieKey) {
    final String filterDept = deptKey.isNotEmpty
        ? deptKey
        : Hive.box(hivePreferenceBox).get(preferenceDept);
    if (specieKey == 0) {
      return _firestore
          .collection('pet')
          .where('detpKey', isEqualTo: filterDept)
          .orderBy('createdAt', descending: true)
          .snapshots()
          .map((snapshot) {
        return snapshot.docs.map((doc) => PetPost.fromDocument(doc)).toList();
      });
    } else {
      return _firestore
          .collection('pet')
          .where('detpKey', isEqualTo: filterDept)
          .where('specie', isEqualTo: specieKey == 1 ? true : false)
          .orderBy('createdAt', descending: true)
          .snapshots()
          .map((snapshot) {
        return snapshot.docs.map((doc) => PetPost.fromDocument(doc)).toList();
      });
    }
  }
}
