import 'package:firu_adoptions/domain/pet_post/pet_post.dart';

abstract class IPetDataSource {
  Stream<List<PetPost>> getPetPosts(String deptKey, int specieKey);
}
