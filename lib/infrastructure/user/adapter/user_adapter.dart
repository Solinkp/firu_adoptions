import 'package:hive/hive.dart';

part 'user_adapter.g.dart';

@HiveType(typeId: 1, adapterName: 'UserAdapter')
class UserHive {
  @HiveField(0)
  String id;
  @HiveField(1)
  String fullName;
  @HiveField(2)
  String phone;
  @HiveField(3)
  bool whatsapp;
  @HiveField(4)
  String email;
  @HiveField(5)
  DateTime createdAt;

  UserHive({
    required this.id,
    required this.fullName,
    required this.phone,
    required this.whatsapp,
    required this.email,
    required this.createdAt,
  });
}
