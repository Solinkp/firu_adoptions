import 'package:firebase_auth/firebase_auth.dart';

import 'package:firu_adoptions/domain/user/user.dart';

abstract class IAuthRepository {
  Stream<User?> get authStateChange;

  Future<void> signOut();

  Future<void> sendPasswordResetEmail({required String email});

  Future<String?> signInWithEmailAndPassword({required String email, required String password});

  Future<String?> signUpWithEmailAndPassword({
    required String email,
    required String password,
    required FiruUser userInfo,
  });
}
