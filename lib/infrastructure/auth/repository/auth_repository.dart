import 'package:injectable/injectable.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:firu_adoptions/domain/user/user.dart';
import 'package:firu_adoptions/infrastructure/auth/repository/i_auth_repository.dart';
import 'package:firu_adoptions/infrastructure/auth/data_source/i_auth_data_source.dart';

@LazySingleton(as: IAuthRepository)
class AuthRepository implements IAuthRepository {
  final IAuthDataSource _authDataSource;

  const AuthRepository(this._authDataSource);

  @override
  Stream<User?> get authStateChange => _authDataSource.authStateChange;

  @override
  Future<void> signOut() async => _authDataSource.signOut();

  @override
  Future<void> sendPasswordResetEmail({required String email}) async =>
      _authDataSource.sendPasswordResetEmail(email: email);

  @override
  Future<String?> signInWithEmailAndPassword({
    required String email,
    required String password,
  }) async =>
      await _authDataSource.signInWithEmailAndPassword(
        email: email,
        password: password,
      );

  @override
  Future<String?> signUpWithEmailAndPassword({
    required String email,
    required String password,
    required FiruUser userInfo,
  }) async =>
      await _authDataSource.signUpWithEmailAndPassword(
        email: email,
        password: password,
        userInfo: userInfo,
      );
}
