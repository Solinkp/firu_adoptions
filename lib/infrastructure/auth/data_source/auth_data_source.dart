import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:firu_adoptions/generated/l10n.dart';
import 'package:firu_adoptions/domain/user/user.dart';
import 'package:firu_adoptions/utils/auth_exceptions.dart';
import 'package:firu_adoptions/utils/constants/constants.dart';
import 'package:firu_adoptions/infrastructure/user/adapter/user_adapter.dart';
import 'package:firu_adoptions/infrastructure/auth/data_source/i_auth_data_source.dart';

@LazySingleton(as: IAuthDataSource)
class AuthDataSource implements IAuthDataSource {
  final FirebaseAuth _firebaseAuth;
  final FirebaseFirestore _firestore;

  const AuthDataSource(this._firebaseAuth, this._firestore);

  @override
  Stream<User?> get authStateChange => _firebaseAuth.authStateChanges();

  @override
  Future<void> signOut() async {
    final userBox = Hive.box<UserHive>(hiveUserBox);
    userBox.clear();
    return _firebaseAuth.signOut();
  }

  @override
  Future<void> sendPasswordResetEmail({required String email}) async =>
      _firebaseAuth.sendPasswordResetEmail(email: email);

  @override
  Future<String?> signInWithEmailAndPassword({
    required String email,
    required String password,
  }) async {
    try {
      UserCredential credential = await _firebaseAuth
          .signInWithEmailAndPassword(email: email, password: password);
      return await _processCredentials(credential);
    } on FirebaseAuthException catch (exception) {
      return AuthExceptions.getAuthErrorMessage(exception.code.toString());
    }
  }

  Future<String?> _processCredentials(UserCredential credential) async {
    User? user = credential.user;
    if (user != null) {
      if (!user.emailVerified) {
        await user.sendEmailVerification();
        signOut();
        return S.current.authVerifyEmail;
      } else {
        await _getUserFromDatabase(user.uid);
      }
    }
    return null;
  }

  Future<void> _getUserFromDatabase(String userId) async {
    final user = await _firestore.collection('users').doc(userId).get();
    final localUser = UserHive(
      id: user.id,
      fullName: user.data()!['fullName'],
      phone: user.data()!['phone'],
      whatsapp: user.data()!['whatsapp'],
      email: user.data()!['email'],
      createdAt: user.data()!['createdAt'].toDate(),
    );
    final userBox = Hive.box<UserHive>(hiveUserBox);
    await userBox.add(localUser);
  }

  @override
  Future<String?> signUpWithEmailAndPassword({
    required String email,
    required String password,
    required FiruUser userInfo,
  }) async {
    try {
      UserCredential credential = await _firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password);
      return await _processSignUpCredentials(credential, userInfo);
    } on FirebaseAuthException catch (exception) {
      return AuthExceptions.getAuthErrorMessage(exception.code.toString());
    }
  }

  Future<String?> _processSignUpCredentials(
      UserCredential credential, FiruUser userInfo) async {
    User? user = credential.user;
    if (user != null) {
      if (!user.emailVerified) {
        await _createUser(user, userInfo);
        await user.sendEmailVerification();
        signOut();
        return S.current.authVerifyEmail;
      }
    }
    return null;
  }

  Future<void> _createUser(User user, FiruUser userInfo) async {
    await _firestore.collection('users').doc(user.uid).set({
      'fullName': userInfo.fullName,
      'phone': userInfo.phone,
      'whatsapp': userInfo.whatsapp,
      'email': user.email,
      'createdAt': userInfo.createdAt,
    });
  }
}
