import 'package:logger/logger.dart';

/// Logger instance with configuration.
final logger = Logger(
  filter: DevelopmentFilter(),
  printer: PrettyPrinter(
    printTime: true,
    colors: false,
    printEmojis: true,
  ),
  output: ConsoleOutput(),
);
