import 'package:firu_adoptions/generated/l10n.dart';

/// Location
const String nicPhoneCode = '+505';
const List<String> nicDepartments = [
  'Boaco',
  'Chinandega',
  'Estelí',
  'Granada',
  'Jinotega',
  'Carazo',
  'Chontales',
  'León',
  'Madriz',
  'Managua',
  'Masaya',
  'Matagalpa',
  'Nueva Segovia',
  'Costa Caribe Sur',
  'Costa Caribe Norte',
  'Rivas',
  'Río San Juan'
];

/// Pets
const List<Map<String, int>> petSpecies = [
  {'All': 0},
  {'Dog': 1},
  {'Cat': 2},
];
List<String> petSpeciesText = [
  S.current.allSpecies,
  S.current.dog,
  S.current.cat,
];

/// Contact method
enum ContactMethodEnum { adoptionForm, phone }

/// Assets
const String assetFiruLogoClearSvg =
    'assets/images/adopt_a_firu_icon_clear.svg';
const String assetFiruLogoClear = 'assets/images/adopt_a_firu_icon_clear.png';
const String assetFiruLogoSvg = 'assets/images/adopt_a_firu_icon.svg';
const String assetFiruLogo = 'assets/images/adopt_a_firu.png';

/// Boxes
const String hivePreferenceBox = 'hivePreferenceBox';
const String preferenceDept = 'preferenceDepartment';
const String preferencePetSpecie = 'preferencePetSpecie';
const String preferenceLanguage = 'preferenceLanguage';
const String preferenceRoute = 'preferenceLanguage';
const String hiveUserBox = 'userBox';
