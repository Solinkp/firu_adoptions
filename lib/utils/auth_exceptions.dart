import 'package:firu_adoptions/generated/l10n.dart';

class AuthExceptions {
  static String getAuthErrorMessage(String code) {
    String errorMessage = '';
    switch (code) {
      case 'invalid-email':
        errorMessage = S.current.authInvalidCredentials;
        break;
      case 'wrong-password':
        errorMessage = S.current.authInvalidCredentials;
        break;
      case 'user-not-found':
        errorMessage = S.current.authUserNotFound;
        break;
      case 'too-many-requests':
        errorMessage = S.current.authBlocked;
        break;
      case 'user-disabled':
        errorMessage = S.current.authDisabled;
        break;
      case 'email-already-in-use':
        errorMessage = S.current.authEmailRegistered;
        break;
      default:
        errorMessage = S.current.authError;
    }
    return errorMessage;
  }
}
