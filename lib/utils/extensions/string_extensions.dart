import 'package:flutter/material.dart';
import 'package:firu_adoptions/generated/l10n.dart';

extension StringExtension on String {
  String? validateEmpty(BuildContext context) => (isEmpty) ? S.of(context).required : null;

  String? validateEmail(BuildContext context) {
    var regex = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    return (isEmpty)
        ? S.of(context).required
        : (!regex.hasMatch(this))
            ? S.of(context).invalidEmail
            : null;
  }
}
